import { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import ContainerBase from "components/ContainerBase";
import ListOfProducts from "components/ListOfProducts";
import modal from "components/Modal";
import postDataVisitor from "../services/postDataVisitor";
import Context from "Context/Context";
import Paginator from "components/Paginator";

export default function Home() {
  const state = useContext(Context);
  const { firstTime, setFirsTime } = state;
  const [searchText, setSearchText] = useState(null);
  const router = useRouter();

  const getData = async () => {
    try {
      const data = await postDataVisitor();
      if (data !== undefined) {
        modal(data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (firstTime) getData();
    setFirsTime(false);
  }, []);

  const { search } = router.query;
  useEffect(() => {
    setSearchText(search);
  }, [search]);

  return (
    <>
      <Head>
        <title>Gapsy</title>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ContainerBase>
        <ListOfProducts searchText={searchText && searchText} />

        <Paginator />
      </ContainerBase>
    </>
  );
}
