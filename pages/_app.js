import { useState } from "react";
import Context from "Context/Context";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  const { Provider } = Context;
  const [ShoppingCartList, setShoppingCartList] = useState([]);
  const [productsBySearch, setProductsBySearch] = useState([]);
  const [pagination, setPagination] = useState(1);
  const [productsList, setProductsList] = useState([]);
  const [firstTime, setFirsTime] = useState(true);

  const state = {
    ShoppingCartList,
    setShoppingCartList,
    productsBySearch,
    setProductsBySearch,
    pagination,
    setPagination,
    productsList,
    setProductsList,
    firstTime,
    setFirsTime,
  };

  return (
    <Provider value={state}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
