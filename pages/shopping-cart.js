import ContainerBase from "components/ContainerBase";
import ListOfProducts from "components/ListOfProducts";
import { typesOfProductsList } from 'utils/DictionaryProductsList'

export default function ShoppingCart() {

  return <ContainerBase>
      <ListOfProducts type={typesOfProductsList.shoppingCart}/>
    </ContainerBase>
}
