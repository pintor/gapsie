import { apiConfig } from "./apiConfig";

export default function postDataVisitor() {
  return fetch(`${apiConfig.url}/visitor`, { method: "POST" })
    .then((data) => data.json())
    .then(data => data)
    .catch(console.error);
}
