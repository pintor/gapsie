import { apiConfig } from "./apiConfig";

export default function getProductsList(category = "reloj", page = "1") {
  return fetch(`${apiConfig.url}/productos/${category}/${page}`)
    .then((data) => data.json())
    .then((data) => data.data)
    .catch(console.error);
}
