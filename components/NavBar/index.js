import { useState, useContext } from "react";
import { AppBar, Toolbar, IconButton, InputBase } from "@material-ui/core";
import Badge from "@material-ui/core/Badge";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import RefreshIcon from "@material-ui/icons/Refresh";
import { useStyles } from "./styles";
import { useRouter } from "next/router";
import Link from "next/link";
import Context from "Context/Context";

export default function NavBar() {
  const state = useContext(Context);
  const { ShoppingCartList } = state;
  const [searchText, setSearchText] = useState("");
  const router = useRouter();
  const classes = useStyles();

  const handleSubmit = (evt) => {
    evt.preventDefault();
    router.push({
      pathname: "/",
      query: { search: searchText },
    });
  };

  return (
    <div className={classes.grow}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <div>
            <Link href="/">
              <a>
                <img src="/logo-white.png" className={classes.icon} />
              </a>
            </Link>
          </div>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <form onSubmit={handleSubmit}>
              <InputBase
                placeholder="Search…"
                onChange={(evt) => setSearchText(evt.target.value)}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ "aria-label": "search" }}
              />
            </form>
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <Link href="/shopping-cart">
              <a>
                <IconButton
                  aria-label="show 17 new notifications"
                  color="inherit"
                >
                  <Badge
                    badgeContent={ShoppingCartList.length}
                    color="secondary"
                  >
                    <ShoppingCartIcon />
                  </Badge>
                </IconButton>
              </a>
            </Link>
            <a href="/">
              <IconButton
                aria-label="show 17 new notifications"
                color="inherit"
              >
                <RefreshIcon />
              </IconButton>
            </a>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
