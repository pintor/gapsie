import { useState, useContext } from "react";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Context from "Context/Context";

export default function ProductCard({ product, classes, disableAddToCart }) {
  const state = useContext(Context);
  const {
    ShoppingCartList,
    setShoppingCartList,
    setProductsList,
    productsBySearch,
  } = state;

  const [anchorEl, setAnchorEl] = useState(null);
  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const addToShoppingCart = () => {
    const allProductsInShoppingCart = [...ShoppingCartList, product];

    setShoppingCartList(allProductsInShoppingCart);
    const shoppingCartListIds = allProductsInShoppingCart.map(
      (product) => product.ID
    );

    let filterProducts = productsBySearch.filter((product) => {
      const isDiferentProduct = shoppingCartListIds.includes(product.ID);
      if (!isDiferentProduct) return product;
    });

    setProductsList(filterProducts);

    handleMenuClose();
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={addToShoppingCart}>Add to shopping cart</MenuItem>
    </Menu>
  );

  const { ID, IMAGE, NAME, DESCRIPTION, PRICE, TYPE } = product;

  return (
    <>
      <GridListTile key={ID} style={{ maxWidth: 250, margin: "7px" }}>
        <img src={IMAGE} alt={NAME} style={{ width: 250 }} />
        <GridListTileBar
          title={
            <span style={{ display: "block" }}>
              {NAME} - {PRICE}
            </span>
          }
          subtitle={<span>{DESCRIPTION}</span>}
          style={{ height: "30%" }}
          actionIcon={
            <IconButton
              aria-label={DESCRIPTION}
              className={classes.icon}
              onClick={handleProfileMenuOpen}
            >
              <MoreVertIcon />
            </IconButton>
          }
        />
      </GridListTile>

      {disableAddToCart && renderMenu}
    </>
  );
}
