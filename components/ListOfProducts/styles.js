import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    // alignSelf: 'center',
    overflow: 'hidden',
    // backgroundColor: theme.palette.background.paper,
    marginTop: '25px'
  },
  gridList: {
    // width: 500,
    // height: 450,
    justifyContent: 'center',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));