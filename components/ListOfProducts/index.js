import { useEffect, useContext } from "react";
import GridList from "@material-ui/core/GridList";
import Alert from "@material-ui/lab/Alert";
import { useStyles } from "./styles";
import getProductsList from "services/getProductsList";
import ProductCard from "components/ProductCard";
import { typesOfProductsList } from "utils/DictionaryProductsList";
import Context from "Context/Context";

export default function ListOfProducts({
  type = typesOfProductsList.searchProducts,
  searchText,
}) {
  const state = useContext(Context);

  const {
    ShoppingCartList,
    setProductsBySearch,
    productsList,
    setProductsList,
    pagination,
  } = state;

  const getData = async () => {
    try {
      const data = await getProductsList(
        searchText ? searchText : "reloj",
        pagination
      );
      if (data !== undefined) {
        setProductsBySearch(data.products);

        const shoppingCartListIds = ShoppingCartList.map(
          (product) => product.ID
        );

        let filterProducts = data.products.filter((product) => {
          const isDiferentProduct = shoppingCartListIds.includes(product.ID);
          if (!isDiferentProduct) return product;
        });

        setProductsList(filterProducts);
      }
    } catch (error) {
      console.error(error);
    }
  };

  if (type === typesOfProductsList.searchProducts) {
    useEffect(() => {
      getData();
    }, [searchText, pagination]);
  } else {
    useEffect(() => {
      setProductsList(ShoppingCartList);
    }, [ShoppingCartList]);
  }

  const classes = useStyles();

  let emptyMessage = () => {
    if (type === typesOfProductsList.searchProducts) {
      return "The list of products is empty, try again";
    } else return "The list of shopping cart is empty";
  };

  return (
    <div className={classes.root}>
      {productsList && productsList.length ? (
        <GridList cols={4} className={classes.gridList}>
          {productsList.map((product) => (
            <ProductCard
              key={`${product.ID}_${product.IMAGE}`}
              product={product}
              classes={classes}
              disableAddToCart={
                type === typesOfProductsList.searchProducts ? true : false
              }
            />
          ))}
        </GridList>
      ) : (
        <Alert severity="warning">{emptyMessage()}</Alert>
      )}
    </div>
  );
}
