import React from "react";
import swal from "@sweetalert/with-react";
import Typography from "@material-ui/core/Typography";

export default function modal({ data }) {
  if (data) {
    const { type, version, visitorId, welcome } = data;
    return swal(
      <div>
        <Typography>{welcome}</Typography>
        <Typography>{type}</Typography>
        <Typography>{visitorId}</Typography>
        <Typography>{version}</Typography>
      </div>
    );
  }
}
