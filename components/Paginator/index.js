import { useContext } from "react";
import Pagination from "@material-ui/lab/Pagination";
import Context from "Context/Context";

export default function Paginator() {
  const state = useContext(Context);
  const { pagination, setPagination } = state;

  const handleChange = (event, value) => {
    setPagination(value);
  };

  const paginationStyles = {
    placeContent: "center",
    display: "grid",
    marginTop: "43px",
    marginBottom: "15px",
  };

  return (
    <div>
      <Pagination
        count={3}
        page={pagination}
        onChange={handleChange}
        style={paginationStyles}
      />
    </div>
  );
}
