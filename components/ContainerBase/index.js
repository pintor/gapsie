import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import NavBar from "components/NavBar";

export default function BaseContainer({ children }) {
  return (
    <>
      <NavBar />
      <CssBaseline />
      <Container maxWidth="lg">{children}</Container>
    </>
  );
}
